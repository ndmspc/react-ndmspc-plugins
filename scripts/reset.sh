#!/bin/bash

PROJECT_DIR="$(dirname $(dirname $(readlink -m $0)))"
cd $PROJECT_DIR
rm -rf dist node_modules package-lock.json yarn.lock
#cd $PROJECT_DIR/example
#rm -rf node_modules  package-lock.json yarn.lock
