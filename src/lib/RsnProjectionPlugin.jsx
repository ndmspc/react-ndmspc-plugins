import { AppGlobalScope, Distributor } from "@ndmspc/react-ndmspc-core";
import { urlExists, getBinId, submitSalsaJob } from "@ndmspc/react-ndmspc-core";
import { getFileObjectNames } from "@ndmspc/react-ndmspc-core";

const RsnProjectionPlugin = new AppGlobalScope({
  name: "RsnProjectionPlugin",
  useCache: false,
  cluster: null,
  isVR: false,
  tools: {
    bin: {
      title: "Bin type",
      options: [
        ["x"],
        ["x", "y", "xy", "yx"],
        ["x", "y", "z", "xy", "yx", "xz", "zx", "yz", "zy", "xyz"],
      ],
    },
  },
  selectedTools: new Map(),
  selectedBins: new Map(),
  // jobs: new Map(),
  workspaces: {
    default: [
      // {
      //   type: "ndmvr",
      //   span: 3,
      //   rowSpan: 2,
      // },
    ],
  },
});

RsnProjectionPlugin.addDistributor(new Distributor("binObjects"));
RsnProjectionPlugin.addDistributor(new Distributor("job"));
RsnProjectionPlugin.addDistributor(new Distributor("selectedPluginTool"));
RsnProjectionPlugin.addDistributor(new Distributor("cluster"));

const handleSlectedPluginTool = (data) => {
  RsnProjectionPlugin.state?.selectedTools.set(data.name, data.value);
};
RsnProjectionPlugin.addHandlerFunc(
  "selectedPluginTool",
  handleSlectedPluginTool,
  null
);
RsnProjectionPlugin.createSubscription("selectedPluginTool");

const handleClusterChange = (data) => {
  RsnProjectionPlugin.state.cluster = data;
};
RsnProjectionPlugin.addHandlerFunc("cluster", handleClusterChange, null);
RsnProjectionPlugin.createSubscription("cluster");

RsnProjectionPlugin.userFunctions = {
  // onBinHover: (data) => {
  //   console.log("NDMSPC RsnProjectionPlugin : onBinHover()", data);
  // },
  onBinClick: async (data) => {
    console.log("NDMSPC RsnProjectionPlugin : onBinClick()", data);

    const clusterInfo = RsnProjectionPlugin.state.cluster;
    // console.log(clusterInfo);
    const selectedTools = RsnProjectionPlugin.state?.selectedTools;
    const binOpt = selectedTools.get("bin");
    // console.log(selectedTools);
    const histogram = data.obj;
    // console.log(histogram);
    const { analysis } = data;

    let { binx = 0, biny = 0, binz = 0 } = data;
    if (histogram._typename.startsWith("TH1")) binx = data.bin + 1;
    console.log(binx,biny,binz)

    const currentProjection = analysis.projections[analysis.projection];
    // console.log(currentProjection);
    // console.log(analysis);

    let idGraphAxis = 0;
    let idStackAxis = 1;
    let idCanvasAxis = -1;
    let graphFilterStr = "";
    let stackFilterStr = "";
    let canvasFilterStr = "";

    if (binOpt === "x") {
      idGraphAxis = 0;
      idStackAxis = -1;
      idCanvasAxis = -1;
    } else if (binOpt === "y") {
      idGraphAxis = 1;
      idStackAxis = -1;
      idCanvasAxis = -1;
    } else if (binOpt === "z") {
      idGraphAxis = 2;
      idStackAxis = -1;
      idCanvasAxis = -1;
    } else if (binOpt === "xy") {
      idGraphAxis = 0;
      idStackAxis = 1;
      idCanvasAxis = -1;
    } else if (binOpt === "yx") {
      idGraphAxis = 1;
      idStackAxis = 0;
      idCanvasAxis = -1;
    } else if (binOpt === "xz") {
      idGraphAxis = 0;
      idStackAxis = 2;
      idCanvasAxis = -1;
    } else if (binOpt === "zx") {
      idGraphAxis = 2;
      idStackAxis = 0;
      idCanvasAxis = -1;
    } else if (binOpt === "yz") {
      idGraphAxis = 1;
      idStackAxis = 2;
      idCanvasAxis = -1;
    } else if (binOpt === "zy") {
      idGraphAxis = 2;
      idStackAxis = 1;
      idCanvasAxis = -1;
    } else if (binOpt === "xyz") {
      idGraphAxis = 0;
      idStackAxis = 1;
      idCanvasAxis = 2;
      if (histogram._typename.startsWith("TH1")) {
        idStackAxis = -1;
        idCanvasAxis = -1;
      } else if (histogram._typename.startsWith("TH2")) {
        idGraphAxis = 0;
        idCanvasAxis = -1;
      } else if (histogram._typename.startsWith("TH3")) {
      }
    } else {
      console.log("Unsupported option : ", binOpt);
      return;
    }

    const base = `${analysis.base.path}/${currentProjection.name}/results`;

    if (histogram._typename.startsWith("TH1")) {
      idStackAxis = -1;
      idCanvasAxis = -1;
    } else if (histogram._typename.startsWith("TH2")) {
      if (idStackAxis === -1) {
        if (idGraphAxis == 0) {
          idStackAxis = 1;
          stackFilterStr = `${biny}`;
        } else if (idGraphAxis == 1) {
          idStackAxis = 0;
          stackFilterStr = `${binx}`;
        }
      }
      idCanvasAxis = -1;
    } else if (histogram._typename.startsWith("TH3")) {
    }

    let proj_extra =
      stackFilterStr.length > 0
        ? `_${stackFilterStr.split(",").join("_")}`
        : "";

    let f = `${analysis.base.http}${base}/proj_${binOpt}${proj_extra}.root`;
    const extra = {
      filename: f,
    };
    const outputFileTmpl = `${analysis.base.root}/${base}/proj_${binOpt}${proj_extra}.root`;

    // console.log(f);

    if (RsnProjectionPlugin.state.useCache && (await urlExists(f))) {
      await getFileObjectNames(f, RsnProjectionPlugin);

      // console.log(`Url ${f} exists`);
    } else {
      RsnProjectionPlugin.getDistributorById("binObjects")?.sendOutputData({
        names: [],
        filename: "",
      });

      const input = `${analysis.base.root}/${analysis.base.path}/${currentProjection.name}/hProjMap.root`;
      const binsDir = `${analysis.base.root}/${analysis.base.path}/${currentProjection.name}/bins`;
      const paramHistogram = "peak";
      const mapStr = "hProjMap";
      const contentFileName = "content.root";
      const filterParseToken = ",";

      // const commands = [];
      const commands = [
        `/opt/ndmspc/ndmspc.sh root -b -q '/opt/ndmspc/NdmspcExportParams.C(${idGraphAxis}, ${idStackAxis},${idCanvasAxis},"${graphFilterStr}", "${stackFilterStr}","${canvasFilterStr}","${input}","${binsDir}","${paramHistogram}","${outputFileTmpl}","${mapStr}","${contentFileName}","${filterParseToken}")'`,
      ];
      console.log(commands);
      const jobDistributor = RsnProjectionPlugin.getDistributorById("job");
      console.log(jobDistributor)
      if (clusterInfo?.executorUrl) {
        // console.log(clusterInfo?.executorUrl);
        
        await submitSalsaJob(
          clusterInfo?.executorUrl,
          commands,
          clusterInfo.submitUrl,
          analysis
        )
          .then((res) => {
            console.log(res);
            if (jobDistributor)
              jobDistributor.sendOutputData({
                res,
                extra,
              });
          })
          .catch((err) => {
            console.error(err);
            if (jobDistributor)
              jobDistributor.sendOutputData({
                error: err,
                extra: clusterInfo,
              });
          });
      } else {
        console.log(clusterInfo);
        console.log("No executor");
        if (jobDistributor)
          jobDistributor.sendOutputData({
            error: "No Executor",
            extra: clusterInfo,
          });
      }
    }
  },
  // onBinDblClick: (data) => {
  //   console.log("NDMSPC RsnProjectionPlugin : onBinDblClick()", data);
  // },
  // onViewChange: (data) => {
  //   console.log("NDMSPC RsnProjectionPlugin : onViewChange()", data);
  // },
  // onExecuteCommand: (data) => {
  //   console.log("NDMSPC RsnProjectionPlugin : onExecuteCommand()", data);
  // },
  // onProgress: (data) => {
  //   console.log("NDMSPC RsnProjectionPlugin : onProgress()", data);
  // },
  // onContextOption: (data) => {
  //   console.log("NDMSPC RsnProjectionPlugin : onContextOption()", data);
  // }
};

export { RsnProjectionPlugin };
