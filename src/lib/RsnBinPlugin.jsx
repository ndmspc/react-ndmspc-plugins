import { AppGlobalScope, Distributor } from "@ndmspc/react-ndmspc-core";
import {
  urlExists,
  getBinId,
  submitSalsaJob,
  root2http,
} from "@ndmspc/react-ndmspc-core";
import { getFileObjectNames } from "@ndmspc/react-ndmspc-core";

const RsnBinPlugin = new AppGlobalScope({
  name: "RsnBinPlugin",
  configuration: new Map(),
  useCache: true,
  cluster: null,
  isVR: false,
  tools: {
    bin: {
      title: "Bin selection",
      type: "select",
      tooltip: "Bin selection type",
      options: [
        ["single", "all"],
        ["single", "all"],
        ["single", "all"],
      ],
    },
    refresh: {
      title: "Refresh maping",
      type: "icon",
      icon: "sync",
      tooltip: "Forcing main histogram to be updated with upstream",
      options: [[], [], []],
    },
    configuration: {
      title: "Configuration",
      type: "configuration",
      tooltip: "Manage configuration",
      options: [[], [], []],
    },
  },
  schemas: [
    {
      name: "Rsn",
      schema: {
        openAPIV3Schema: {
          description: "Resonance configuration",
          properties: {
            inputfile: {
              default:
                "root://eos.ndmspc.io//eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_pp_AOD/987/phi_leading_3s/AnalysisResults.root",
              description: "Input filename to process",
              type: "string",
            },
            sigbg: {
              default: "Unlike",
              description: "Signal + background",
              type: "string",
            },
            bg: {
              default: "Mixing",
              description: "Background",
              type: "string",
            },
            minentries: {
              default: 10000,
              description: "Minimum number of entries in 'sb' histogram",
              type: "integer",
            },
          },
        },
      },
    },
  ],
  selectedTools: new Map(),
  selectedBins: new Map(),
  // jobs: new Map(),
  workspaces: {
    default: [
      // {
      //   type: "ndmvr",
      //   span: 4,
      //   rowSpan: 2,
      // },
      // {
      //   type: "ndmtabs",
      //   span: 2,
      //   rowSpan: 1,
      // },
    ],
  },
});

RsnBinPlugin.addDistributor(new Distributor("binSelect"));
RsnBinPlugin.addDistributor(new Distributor("binObjects"));
RsnBinPlugin.addDistributor(new Distributor("job"));
RsnBinPlugin.addDistributor(new Distributor("pluginPipe"));
RsnBinPlugin.addDistributor(new Distributor("selectedPluginTool"));
RsnBinPlugin.addDistributor(new Distributor("cluster"));
RsnBinPlugin.addDistributor(new Distributor("configuration"));

const handleConfiguration = (data) => {
  RsnBinPlugin.state?.configuration.set("default", data);
};
RsnBinPlugin.addHandlerFunc("configuration", handleConfiguration, null);
RsnBinPlugin.createSubscription("configuration");

const handleSlectedPluginTool = (data) => {
  RsnBinPlugin.state?.selectedTools.set(data.name, data.value);
};
RsnBinPlugin.addHandlerFunc(
  "selectedPluginTool",
  handleSlectedPluginTool,
  null
);
RsnBinPlugin.createSubscription("selectedPluginTool");

const handleClusterChange = (data) => {
  RsnBinPlugin.state.cluster = data;
};
RsnBinPlugin.addHandlerFunc("cluster", handleClusterChange, null);
RsnBinPlugin.createSubscription("cluster");

const handleBinSelectFunc = (binData) => {
  console.log(binData);
  if (binData) {
    const selectedBin = RsnBinPlugin.state.selectedBins.get(binData.id);
    if (selectedBin) {
      RsnBinPlugin.state.selectedBins.delete(binData.id);
    } else {
      RsnBinPlugin.state.selectedBins.set(binData.id, binData.data);
    }
    console.log(Array.from(RsnBinPlugin.state.selectedBins.keys()));
    RsnBinPlugin.getDistributorById("binSelect")?.sendOutputData(
      Array.from(RsnBinPlugin.state.selectedBins.keys())
    );
  }
};

RsnBinPlugin.addHandlerFunc("binSelect", handleBinSelectFunc, null);

RsnBinPlugin.userFunctions = {
  // onBinHover: (data) => {
  // console.log("NDMSPC RsnBinPlugin : onBinHover()", data, RsnBinPlugin.state);
  // },
  onScanHistogram: async (data) => {
    console.log("NDMSPC RsnBinPlugin : onScanHistogram()", data);

    const { analysis } = data;
    const currentProjection = analysis.projections[analysis.projection];

    const clusterInfo = RsnBinPlugin.state.cluster;
    // console.log(clusterInfo);
    const selectedTools = RsnBinPlugin.state?.selectedTools;
    // console.log(selectedTools);

    const hProjMapDir = `${analysis.base.root}${analysis.base.path}/${currentProjection.name}`;
    const binContent = 2;
    const defaultFill = 1;

    const extra = {
      canvas: "workspace-main",
      filename: `${hProjMapDir}/hProjMap.root`,
      bin: selectedTools.get("bin"),
    };

    const commands = [
      `/opt/ndmspc/ndmspc.sh root -b -q '/opt/ndmspc/NdmspcScanProjectionMap.C("${hProjMapDir}",${binContent},${defaultFill})'`,
    ];
    console.log(commands);

    if (clusterInfo?.executorUrl) {
      // console.log(clusterInfo?.executorUrl);

      await submitSalsaJob(
        clusterInfo?.executorUrl,
        commands,
        clusterInfo.submitUrl,
        analysis
      ).then((res) => {
        console.log(res, extra);
        RsnBinPlugin.getDistributorById("job")?.sendOutputData({
          res,
          extra,
        });
      });
    } else {
      console.log(clusterInfo);
      console.log("No executor");
      RsnBinPlugin.getDistributorById("job")?.sendOutputData({
        error: "No Executor",
        extra: clusterInfo,
      });
    }
  },
  onBinClick: async (data) => {
    console.log(
      "NDMSPC RsnBinPlugin : onBinClick()",
      data,
      RsnBinPlugin?.state?.configuration
    );

    const clusterInfo = RsnBinPlugin.state.cluster;
    // console.log(clusterInfo);
    const selectedTools = RsnBinPlugin.state?.selectedTools;
    // console.log(selectedTools);

    const { analysis } = data;
    const histogram = data.obj;

    let { binx = 0, biny = 0, binz = 0 } = data;
    // TODO Fix binx = data.bin + 1 propertly
    if (histogram._typename.startsWith("TH1") && !data.isVR) binx = data.bin + 1;
    // console.log(binx, biny, binz);
    // console.log(histogram);
    const currentProjection = analysis.projections[analysis.projection];
    // console.log(currentProjection);
    // console.log(app);
    // console.log(analysis);

    const cutAxisStr = currentProjection.axes.join("_");
    // const cutAxisStr = "1,2";

    if (!RsnBinPlugin?.state?.configuration.get("default")) {
      console.log("Error: No config found !!!");
      return;
    }
    console.log(RsnBinPlugin.state.configuration.get("default"))
    const { properties: rsnBinConfig } =
      RsnBinPlugin.state.configuration.get("default");
    console.log(rsnBinConfig);

    // return;

    const input =
      rsnBinConfig?.inputfile?.value || rsnBinConfig?.inputfile?.default;
    const sigBgStr = rsnBinConfig?.sigbg?.value || rsnBinConfig?.sigbg?.default;
    const bgStr = rsnBinConfig?.bg?.value || rsnBinConfig?.bg?.default;
    const minEntries =
      rsnBinConfig?.minentries?.value || rsnBinConfig?.minentries?.default;

    // Improve to get ip (https://www.npmjs.com/package/fetch-dns-lookup)
    // const input = `${analysis.base.root}/eos/ndmspc/scratch/alice/cern.ch/user/a/alihyperloop/outputs/128019/14984/AnalysisResults.root`;
    // const sigBgStr = "rsnanalysis-t-hn-sparse/unlike";
    // const bgStr = "rsnanalysis-t-hn-sparse/likep";

    // const input = `${analysis.base.root}/eos/ndmspc/scratch/alice.cern.ch/user/a/alitrain/PWGLF/LF_pp_AOD/987/phi_leading_3s/AnalysisResults.root`;
    // const sigBgStr = "Unlike";
    // const bgStr = "Mixing";

    const outHost = `${analysis.base.root}`;
    // const outHostHttp = root2http(analysis.base.root)[0];
    // const outHost = "root://147.213.204.139";
    // const outHost = "root://localhost/";
    const outDir = `${analysis.base.path}/${currentProjection.name}/bins`;
    const outputFileName = "content.root";
    const idAxis = 0;

    let commands = [];

    let f;
    if (histogram._typename.startsWith("TH1")) {
      f = `${analysis.base.http}/${analysis.base.path}/${currentProjection.name}/bins/${binx}/content.root`;
    } else if (histogram._typename.startsWith("TH2")) {
      f = `${analysis.base.http}/${analysis.base.path}/${currentProjection.name}/bins/${binx}/${biny}/content.root`;
    } else if (histogram._typename.startsWith("TH3")) {
      f = `${analysis.base.http}/${analysis.base.path}/${currentProjection.name}/bins/${binx}/${biny}/${binz}/content.root`;
    } else {
      console.log("Unsuported histogram ", histogram);
      return;
    }

    const extra = {
      canvas: "workspace-content",
      filename: f,
      bin: selectedTools.get("bin"),
    };

    if (selectedTools.get("bin") === "single") {
      // console.log(f);
      if (RsnBinPlugin.state.useCache && (await urlExists(f))) {
        await getFileObjectNames(f, RsnBinPlugin);
        return;
        // console.log(`Url ${f} exists`);
        // } else if (await urlExists(fBase)) {

        //   await getNdmspcFileProjection(fBase, fBaseObject);
        //   console.log(`Url ${f} exists`);
      } else {
        RsnBinPlugin.getDistributorById("binObjects")?.sendOutputData({
          names: [],
          filename: "",
        });

        histogram.setBinContent(histogram.getBin(binx, biny, binz), 2);
        RsnBinPlugin.getDistributorById("pluginPipe")?.sendOutputData({
          type: "histogram",
          action: "redraw",
          payload: histogram,
        });

        // const char *cutAxisStr="1,2,3",const char *cutBinMinsStr = "5,4,3",const char *cutBinMaxsStr="5,4,3"

        // const commands = [];
        // const commands = ["sleep 10"];

        if (histogram._typename.startsWith("TH1")) {
          commands = [
            `/opt/ndmspc/ndmspc.sh root -b -q '/opt/ndmspc/RsnBin.C("${cutAxisStr}", "${binx}","${binx}","${input}","${sigBgStr}","${bgStr}","${outHost}","${outDir}","${outputFileName}",${idAxis},${minEntries})'`,
          ];
        } else if (histogram._typename.startsWith("TH2")) {
          commands = [
            `/opt/ndmspc/ndmspc.sh root -b -q '/opt/ndmspc/RsnBin.C("${cutAxisStr}", "${binx},${biny}", "${binx},${biny}","${input}","${sigBgStr}","${bgStr}","${outHost}","${outDir}","${outputFileName}",${idAxis},${minEntries})'`,
          ];
        } else if (histogram._typename.startsWith("TH3")) {
          commands = [
            `/opt/ndmspc/ndmspc.sh root -b -q '/opt/ndmspc/RsnBin.C("${cutAxisStr}", "${binx},${biny},${binz}","${binx},${biny},${binz}","${input}","${sigBgStr}","${bgStr}","${outHost}","${outDir}","${outputFileName}",${idAxis},${minEntries})'`,
          ];
        }

        console.log(commands);
      }
    } else if (selectedTools.get("bin") === "all") {
      RsnBinPlugin.getDistributorById("binObjects")?.sendOutputData({
        names: [],
        filename: "",
      });
      // const char *cutAxisStr="1,2,3",const char *cutBinMinsStr = "5,4,3",const char *cutBinMaxsStr="5,4,3"

      // for over all bins

      if (histogram._typename.startsWith("TH1")) {
        for (let ix = 1; ix <= histogram.fXaxis.fNbins; ix++) {
          commands.push(
            `/opt/ndmspc/ndmspc.sh root -b -q '/opt/ndmspc/RsnBin.C("${cutAxisStr}", "${ix}","${ix}","${input}","${sigBgStr}","${bgStr}","${outHost}","${outDir}","${outputFileName}",${idAxis},${minEntries})'`
          );
        }
      } else if (histogram._typename.startsWith("TH2")) {
        for (let iy = 1; iy <= histogram.fYaxis.fNbins; iy++) {
          for (let ix = 1; ix <= histogram.fXaxis.fNbins; ix++) {
            commands.push(
              `/opt/ndmspc/ndmspc.sh root -b -q '/opt/ndmspc/RsnBin.C("${cutAxisStr}", "${ix},${iy}","${ix},${iy}","${input}","${sigBgStr}","${bgStr}","${outHost}","${outDir}","${outputFileName}",${idAxis},${minEntries})'`
            );
          }
        }
      } else if (histogram._typename.startsWith("TH3")) {
        for (let iz = 1; iz <= histogram.fZaxis.fNbins; iz++) {
          for (let iy = 1; iy <= histogram.fYaxis.fNbins; iy++) {
            for (let ix = 1; ix <= histogram.fXaxis.fNbins; ix++) {
              commands.push(
                `/opt/ndmspc/ndmspc.sh root -b -q '/opt/ndmspc/RsnBin.C("${cutAxisStr}", "${ix},${iy},${iz}","${ix},${iy},${iz}","${input}","${sigBgStr}","${bgStr}","${outHost}","${outDir}","${outputFileName}",${idAxis},${minEntries})'`
              );
            }
          }
        }
      } else {
        console.log("Unsuported histogram ", histogram);
        return;
      }

      console.log(commands);
      // RsnBinPlugin.getDistributorById("selectedPluginTool")?.sendInputData({
      //   name: "bin",
      //   value: "single",
      // });
      // return
    }

    // console.log(zmq2ws);
    if (clusterInfo?.executorUrl) {
      console.log(clusterInfo?.executorUrl);

      await submitSalsaJob(
        clusterInfo?.executorUrl,
        commands,
        clusterInfo.submitUrl,
        analysis
      )
        .then((res) => {
          console.log(res);
          RsnBinPlugin.getDistributorById("job")?.sendOutputData({
            res,
            extra,
          });
        })
        .catch((err) => {
          console.error(err);
        });
    } else {
      console.log(clusterInfo);
      console.log("No executor");
      RsnBinPlugin.getDistributorById("job")?.sendOutputData({
        error: "No Executor",
        extra: clusterInfo,
      });
    }

    // console.log(RsnBinPlugin.getDistributorById("binSelect"));

    RsnBinPlugin.getDistributorById("binSelect")?.sendInputData(getBinId(data));

    // await getFileObjectNames(`https://eos0.ndmspc.io/eos/ndmspc/scratch/test/${binx}/${biny}/out.root`);
    // await getFileObjectNames(
    //   `https://eos0.ndmspc.io/eos/ndmspc/scratch/test/ishaan/${binx}/${biny}/casc_fitting_proj.root`
    // );
  },

  onBinDbClick: (data) => {
    console.log("DbClick ", data);
  },

  onViewChange: (data) => {
    let idData = getBinId(data, "local");
    let finalColor = RsnBinPlugin.state.color ?? "#808080";
    Array.from(RsnBinPlugin.state.selectedBins?.keys()).forEach((x) => {
      if (x === idData.id) {
        finalColor = "#00ffff";
      }
    });
    return finalColor;
  },

  onExecuteCommand: (data) => {
    console.log("onExecuteCommand ", data);
  },
};

export { RsnBinPlugin };
